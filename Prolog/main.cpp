//
//  main.cpp
//  Prolog
//
//  Created by Phillip Voyle on 10/12/13.
//  Copyright (c) 2013 PV. All rights reserved.
//

#include <iostream>
#include "scope.h"
#include <vector>
#include <string>
#include <sstream>

class continuation
{
public:
	virtual bool evaluate(scope& s) = 0;
	bool operator()(scope& s) {return evaluate(s);}
};

class prove_predicate: public continuation
{
	const predicate_t& m_p;
	continuation& m_c;
	const clause_db_t& m_db;
public:
	prove_predicate(const predicate_t& p, continuation& c, const clause_db_t& db):
		m_p(p), m_c(c), m_db(db)
	{
	}
	
	bool evaluate(scope& s);
};

class instantiate_predicate: public continuation
{
	continuation& m_c;
	instantiated_predicate_t& m_ip;
	const predicate_args_t& m_p;
public:
	instantiate_predicate(continuation& c, const predicate_args_t& p, instantiated_predicate_t& ip):
		m_c(c), m_p(p), m_ip(ip)
	{
	}
	bool evaluate(scope& s)
	{
		m_ip.second.clear();
		for(predicate_args_t::const_iterator it = m_p.begin(); it != m_p.end(); it++)
		{
			m_ip.second.push_back(s.instantiate_atom(*it));
		}
		return m_c(s);
	}
};

class unify_predicate: public continuation
{
	continuation& m_c;
	const predicate_args_t& m_p;
	const instantiated_predicate_t& m_ip;
public:
	unify_predicate( continuation& c, const predicate_args_t& p,
		const instantiated_predicate_t& ip):
		m_c(c), m_p(p), m_ip(ip)
	{
	}
	bool evaluate(scope& s)
	{
		int i = 0;
		for(i = 0; i < m_ip.second.size(); i++)
		{
			if(!s.unify_atom(m_p[i], m_ip.second[i]))
			{
				return false;
			}
		}

		s.set_predicate(m_ip, true);
		return m_c(s);
	}
};

class prove_conjunction: public continuation
{
	conjunction_t::const_iterator m_b;
	conjunction_t::const_iterator m_e;
	continuation& m_c;
	const clause_db_t& m_db;
public:

	prove_conjunction(conjunction_t::const_iterator b, conjunction_t::const_iterator e, continuation& c, const clause_db_t& db):
		m_b(b), m_e(e), m_c(c), m_db(db)
	{
	}
	
	bool evaluate(scope& s)
	{
		if(m_b == m_e)
		{
			return m_c(s);
		}
		else
		{
			scope s1(&s, s.get_local_vars()); //needs previous scope
			prove_conjunction conj(m_b + 1, m_e, m_c, m_db);
			prove_predicate pred(*m_b, conj, m_db);
			return pred(s1);
		}
	}
};

class unify_clause: public continuation
{
	const instantiated_predicate_t& m_ip;
	continuation& m_c;
	const clause_db_t& m_db;
	
	const clause_t& clause;
	
public:
	unify_clause(const clause_t& cl, const instantiated_predicate_t& ip, continuation& c, const clause_db_t& db):
		clause(cl),
		m_ip(ip),
		m_c(c),
		m_db(db)
	{
	}
	bool evaluate(scope& s)
	{
		prove_conjunction conj(clause.second.begin(), clause.second.end(), m_c, m_db);
		unify_predicate up(conj, clause.first, m_ip);
		
		if(up(s))
		{
			return true;
		}
		return false;
	}
};

class unify_clause_list: public continuation
{
	const clause_list_t& m_l;
	
	const instantiated_predicate_t& m_ip;
	continuation& m_c;
	const clause_db_t& m_db;
	
public:
	unify_clause_list(const clause_list_t& l, const instantiated_predicate_t& ip, continuation& c, const clause_db_t& db):
		m_l(l),
		m_ip(ip),
		m_c(c),
		m_db(db)
	{
	}
	bool evaluate(scope& s)
	{
		for(clause_list_t::const_iterator it_clause = m_l.begin();
			it_clause != m_l.end(); it_clause++)
		{
			scope new_scope(&s);
			const clause_t& clause = *it_clause;
			
			unify_clause uc(clause, m_ip, m_c, m_db);
			
			if(uc(new_scope))
			{
				return true;
			}
		}
		return false;
	}
};

bool prove_predicate::evaluate(scope& s)
{
	clause_db_t::const_iterator it = m_db.find(m_p.first);
	if(it != m_db.end())
	{
		instantiated_predicate_t ip{m_p.first, instantiated_predicate_args_t{}};
		
		unify_clause_list ucl(it->second, ip, m_c, m_db);
		instantiate_predicate ins(ucl, m_p.second, ip);
		
		return ins(s);
	}
	return false;
}

class complete_list: public continuation
{
	const std::map<std::string, int>& m_v;
public:
	complete_list(const std::map<std::string, int>& v):m_v(v)
	{
	}
	
	bool evaluate(scope& s)
	{
		std::cout << "success:" << std::endl;
		for(std::map<std::string, int>::const_iterator it = m_v.begin(); it != m_v.end(); it++)
		{
			std::cout << it->first << " = " << s.get_str_value(it->second) << std::endl;
		}
		return false;
	}
};

class complete_conj: public continuation
{
	const std::map<std::string, int>& m_v;
public:
	complete_conj(const std::map<std::string, int>& v):m_v(v)
	{
	}
	bool evaluate(scope& s)
	{
		for(std::map<std::string, int>::const_iterator it = m_v.begin(); it != m_v.end(); it++)
		{
			std::cout << it->first << " = " << s.get_str_value(it->second) << std::endl;
		}
		return true;
	}
};

class complete: public continuation
{
	const std::map<std::string, int>& m_v;
public:
	complete(const std::map<std::string, int>& v):m_v(v)
	{
	}
	bool evaluate(scope& s)
	{
		for(std::map<std::string, int>::const_iterator it = m_v.begin(); it != m_v.end(); it++)
		{
			std::cout << it->first << " = " << s.get_str_value(it->second) << std::endl;
		}
		return true;
	}
};

bool prove(const clause_db_t& db, const conjunction_t& conjunction)
{
	scope s;
	
	//initialise atoms
	for(int i = 0; i < conjunction.size(); i++)
	{
		for(int j = 0; j < conjunction[i].second.size(); j++)
		{
			s.instantiate_atom(conjunction[i].second[j]);
		}
	}
	
	complete_conj done(s.get_local_vars());
	prove_conjunction conj(conjunction.begin(), conjunction.end(), done, db);
	return conj(s);
}

bool prove(const clause_db_t& db, const predicate_t& predicate)
{
	scope s;
	complete done(s.get_local_vars());
	
	prove_predicate prove_functor(predicate, done, db);
	return prove_functor(s);
}

void enumerate(const clause_db_t& db, const predicate_t& predicate)
{
	scope s;
	complete_list done(s.get_local_vars());
	
	prove_predicate prove_functor(predicate, done, db);
	prove_functor(s);
}

template<typename...TS>
predicate_args_t load_predicate_args(const TS&...args)
{
	return predicate_args_t{std::string(args)...};
}

void install_clause(clause_db_t& db, const predicate_t & left, const conjunction_t& conjunction)
{
	db[left.first].push_back(std::make_pair(left.second, conjunction));
}

void install_clause(clause_db_t& db, const clause_def_t& clause)
{
	db[clause.first.first].push_back(std::make_pair(clause.first.second, clause.second));
}

void skip_ws(std::istream& is)
{
	for(;;)
	{
		char c = is.peek();
		if((c == '\n') || (c == '\r') || (c == ' ') || (c == '\t'))
		{
			is.get();
		}
		else
		{
			break;
		}
	}
}

std::string parse_atom(std::istream& is)
{
	skip_ws(is);
	std::string result;
	for(;;)
	{
		char c = is.peek();
		if(isalnum(c) || (c == '_'))
		{
			is.get();
			result.push_back(c);
		}
		else
		{
			return result;
		}
	}
}

predicate_t parse_predicate(std::istream& is)
{
	predicate_t result;
	result.first = parse_atom(is);
	if(result.first.length() > 0)
	{
		skip_ws(is);
		if(is.peek() == '(')
		{
			is.get();
		}
		
		for(;;)
		{
			skip_ws(is);
			if(is.peek() == ')')
			{
				is.get();
				return result;
			}
			if((result.second.size() > 0) && (is.get() != ','))
			{
				throw std::runtime_error("expected ','");
			}
			skip_ws(is);
			
			char c = is.peek();
			if(isalnum(c) || (c == '_'))
			{
				result.second.push_back(parse_atom(is));
			}
			else
			{
				throw std::runtime_error("expected a predicate");
			}
		}
	}
	throw std::runtime_error("not a predicate");
};

clause_def_t parse_clause(std::istream& is)
{
	skip_ws(is);
	
	clause_def_t result;
	if(is.peek() == ':')
	{
		//uh-oh..
		is.get();
		if(is.get() != '-')
		{
			throw std::runtime_error("expected ':-'");
		}
	}
	else
	{
		result.first = parse_predicate(is);
		skip_ws(is);
		char c = is.get();
		if(c == '.')
		{
			return result;
		}
		
		if((c != ':') || (is.get() != '-'))
		{
			throw std::runtime_error("expected ':-'");
		}
	}
	
	for(;;)
	{
		skip_ws(is);
		if(is.peek() == '.')
		{
			is.get();
			return result;
		}
		skip_ws(is);
		if(result.second.size() > 0)
		{
			char c = is.get();
			if(c != ',')
				throw std::runtime_error("expected ','");
		}
		skip_ws(is);
		result.second.push_back(parse_predicate(is));
	}
}

predicate_t parse_predicate(const std::string& sPredicate)
{
	std::stringstream ssPredicate(sPredicate);
	return parse_predicate(ssPredicate);
}

clause_def_t parse_clause(const std::string& sClause)
{
	std::stringstream ssClause(sClause);
	return parse_clause(ssClause);
}

void parse_spec(clause_db_t& db, std::istream& is)
{
	for(;;)
	{
		skip_ws(is);
		if(is.peek() == EOF)
		{
			return;
		}
		clause_def_t def = parse_clause(is);
		if(def.first.first == "")
		{
			if((def.second.size() == 1) ? prove(db, def.second[0]) : prove(db, def.second))
			{
				std::cout << "yes." << std::endl;
			}
			else
			{
				std::cout << "no." << std::endl;
			}
		}
		else
		{
			install_clause(db, def);
		}
	}
}

int main(int argc, const char * argv[])
{

	clause_db_t db;
	
	std::string program = ""
	"parent(david, darienne).\n"
	"parent(kay, darienne).\n"
	"parent(alby, ian).\n"
	"parent(mary, ian).\n"
	"parent(ian, phillip).\n"
	"parent(darienne, phillip).\n"
	"parent(ian, tony).\n"
	"parent(darienne, tony).\n"
	"parent(phillip, keira).\n"
	"parent(phillip, blake).\n"
	"ancestor(X, Y) :- parent(X, Y).\n"
	"ancestor(X, Y) :- parent(X, Z), ancestor(Z, Y).\n"
	;

	std::stringstream ssprogram(program);
	parse_spec(db, ssprogram);
	
	predicate_t test = parse_predicate("ancestor(phillip,keira)");
	
	if(prove(db, test))
	{
		std::cout << "success!" << std::endl;
	}
	else
	{
		std::cout << "failure!" << std::endl;
	}
	enumerate(db, parse_predicate("parent(Y, keira)"));
	return 0;
}

