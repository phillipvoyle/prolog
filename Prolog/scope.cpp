//
//  scope.cpp
//  Prolog
//
//  Created by Phillip Voyle on 10/12/13.
//  Copyright (c) 2013 PV. All rights reserved.
//

#include "scope.h"
#include <exception>
#include <iostream>

scope::scope(const scope* p):
	parent_scope(p),
	variable_count(p->variable_count),
	value_count(p->value_count)
{
}
scope::scope():
	parent_scope(nullptr),
	variable_count(0),
	value_count(0)
{
}

scope::scope(const scope* p, const local_vars_t& vars):
	parent_scope(p),
	local_variables(vars),
	variable_count(p->variable_count),
	value_count(p->value_count)
{
}

value_t scope::get_value(int nValueID)
{
	const scope* p = this;
	for(;;)
	{
		values_t::const_iterator it = p->values.find(nValueID);
		if(it != p->values.end())
		{
			return it->second;
		}
		
		p = p->parent_scope;
		if(p == nullptr)
		{
			throw std::logic_error("asked for value that doesn't exist");
		}
	}
}

scope::value_index_t scope::get_value_index(int nVariable)
{
	int nCurrentVariable = nVariable;
	
	const scope* p = this;

	for(;;)
	{
		variables_t::const_iterator it = p->variables.find(nCurrentVariable);
		if(it != p->variables.end())
		{
			if(it->second.first) //is a value
			{
				if(it->second.second == -1)
				{
					return value_index_t(false, nCurrentVariable);
				}
				else
				{
					return it->second;
				}
			}
			else //is a variable
			{
				nCurrentVariable = it->second.second;
				p = this;
			}
		}
		else
		{
			//not in this scope
			p = p->parent_scope;
			if(p == nullptr)
			{
				throw std::logic_error("asked for variable that doesn't exist");
			}
		}
	}
}

int scope::instantiate_variable(const std::string& variable)
{
	local_vars_t::iterator it = local_variables.find(variable);
	if(it == local_variables.end())
	{
		int result = variable_count ++;
		variables[result] = value_index_t(true, -1);
		if(variable != "_")
		{
			local_variables[variable] = result;
		}
		return result;
	}
	else
	{
		return it->second;
	}
}
std::pair<bool, bool> scope::instantiate_predicate(const instantiated_predicate_t& predicate)
{
	const scope* p = this;
	for(;;)
	{
		if(p == nullptr)
		{
			m_predicates[predicate] = false;
			return std::make_pair(false, false);
		}
		predicate_map_t::const_iterator it = p->m_predicates.find(predicate);
		if(it != p->m_predicates.end())
		{
			return std::make_pair(false, it->second);
		}
		else
		{
			p = p->parent_scope;
		}
	}
}

void scope::set_predicate(const instantiated_predicate_t& predicate, bool result)
{
	m_predicates[predicate] = result;
}

int scope::instantiate_value(const std::string& value)
{

	int value_id = value_count ++;
	values[value_id] = value;
	
	int result = variable_count ++;
	variables[result] = value_index_t(true, value_id);
	return result;
}

bool scope::unify_variable(const std::string& variable, int variable_id)
{
	if(variable == "_")
	{
		return true; //why should I care?
	}
	
	local_vars_t::iterator it = local_variables.find(variable);
	if(it == local_variables.end())
	{
		local_variables[variable] = variable_id;
		return true;
	}
	else
	{
		value_index_t old_value = get_value_index(variable_id);
		value_index_t new_value = get_value_index(it->second);
		
		if(new_value.first)
		{
			if(old_value.first)
			{
				return get_value(old_value.second) == get_value(new_value.second);
			}
			else
			{
				variables[old_value.second] = new_value;
			}
		}
		else
		{
			variables[new_value.second] = old_value;
		}
		return true;
	}
}

bool scope::unify_value(const std::string& value, int variable_id)
{
	value_index_t old_value = get_value_index(variable_id);
	if(old_value.first)
	{
		return get_value(old_value.second) == value;
	}
	else
	{
		int value_id = value_count ++;
		values[value_id] = value;
		variables[old_value.second] = value_index_t(true, value_id);
		return true;
	}
}

bool scope::is_variable(const std::string& atom)
{
	return ((atom.length() > 0) && (atom[0] >= 'A') && (atom[0] <= 'Z')) || (atom == "_") ;
}

int scope::instantiate_atom(const std::string &atom)
{
	int variable_id = is_variable(atom) ? instantiate_variable(atom) : instantiate_value(atom);
	return variable_id;
}

bool scope::unify_atom(const std::string &atom, int variable_id)
{
	if(is_variable(atom) ? unify_variable(atom, variable_id) : unify_value(atom, variable_id))
	{
		return true;
	}
	else
	{
		return false;
	}
}

std::string scope::get_str_value(int nVariable)
{
	value_index_t index = get_value_index(nVariable);
	if(index.first)
	{
		return get_value(index.second);
	}
	else
	{
		return "?";
	}
}

