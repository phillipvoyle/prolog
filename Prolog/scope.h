//
//  scope.h
//  Prolog
//
//  Created by Phillip Voyle on 10/12/13.
//  Copyright (c) 2013 PV. All rights reserved.
//

#ifndef Prolog_scope_h
#define Prolog_scope_h



#include "value.h"

class scope
{
	const scope* parent_scope;
	
	typedef std::map<std::string, int> local_vars_t;
	typedef std::pair<bool, int> value_index_t;
	typedef std::map<int, value_index_t> variables_t;
	typedef std::map<int, value_t> values_t;
	
	typedef std::map<instantiated_predicate_t, bool> predicate_map_t;
	predicate_map_t m_predicates;
	
	local_vars_t local_variables;
	variables_t variables;
	values_t values;
	int variable_count;
	int value_count;


	value_t get_value(int nValueID);
	value_index_t get_value_index(int nVariable);
	
	scope(const scope&) = delete;
public:
	const local_vars_t& get_local_vars() {return local_variables;}
	
	static bool is_variable(const std::string& atom);
	
	int instantiate_variable(const std::string& variable);
	int instantiate_value(const std::string& value);
	int instantiate_atom(const std::string& atom);
	
	std::pair<bool, bool> instantiate_predicate(const instantiated_predicate_t& predicate);
	void set_predicate(const instantiated_predicate_t& predicate, bool result);
		
	bool unify_variable(const std::string& variable, int variable_id);
	bool unify_value(const std::string& value, int variable_id);
	bool unify_atom(const std::string& atom, int variable_id);
	
	scope(const scope* p);
	scope();
	scope(const scope* p, const local_vars_t& vars);
	
	std::string get_str_value(int nVariable);
};


#endif
