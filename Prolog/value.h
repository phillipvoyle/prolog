//
//  value.h
//  Prolog
//
//  Created by Phillip Voyle on 10/12/13.
//  Copyright (c) 2013 PV. All rights reserved.
//

#ifndef Prolog_value_h
#define Prolog_value_h

#include <string>
#include <map>
#include <vector>
#include <string>

typedef std::string value_t;

typedef std::vector<std::string> predicate_args_t;
typedef std::pair<std::string, predicate_args_t> predicate_t;
typedef std::vector<predicate_t> conjunction_t;

typedef std::pair<predicate_args_t, conjunction_t> clause_t;

typedef std::pair<predicate_t, conjunction_t> clause_def_t;

typedef std::vector<clause_t> clause_list_t;
typedef std::map<std::string, clause_list_t> clause_db_t;

typedef  std::vector<int> instantiated_predicate_args_t;

typedef std::pair<std::string,instantiated_predicate_args_t> instantiated_predicate_t;

#endif
